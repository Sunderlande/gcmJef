package gcmjef1;

import org.junit.Assert;
import org.junit.Test;

public class FatorialTeste {

	@Test
	public void factorialZero() {
		Fatorial f = new Fatorial();
		int expected = 1;
		int actual = f.fact(0);
		Assert.assertEquals(expected, actual);

	}

	@Test
	public void factorialDois() {
		Fatorial f = new Fatorial();
		int expected = 2;
		int actual = f.fact(2);
		Assert.assertEquals(expected, actual);
	}
}
